/*
 * scripts/js/ - Platon SDG JavaScript library
 *
 * contentTableResize.js - powerful content tables resizing
 * ____________________________________________________________
 *
 * Developed by Ondrej Jombik <nepto AT platon.sk>
 * Copyright (c) 2002-2004 Platon SDG, http://platon.sk/
 * All rights reserved.
 *
 * See README file for more information about this software.
 * See COPYING file for license information.
 *
 * Download the latest version from
 * http://platon.sk/projects/scripts/
 */

/* $Platon: scripts/js/contentTableResize/contentTableResize.js,v 1.7 2004/03/09 13:31:10 nepto Exp $ */

// Default values (edit if needed)

var contentTableMaxClientWidth = 790;
var contentTableNewWidth       = 750;
var contentTableDebug          = 0;
var contentTableDisable        = 0;

// Do not edit

// Array to store original widths of content tables
var contentTablesOriginalWidth = null;

function contentTableResize()
{
	if (contentTableDisable)
		return true;

	if (! document.getElementsByTagName) {
		if (contentTableDebug) {
			alert('document.getElementsByTagName is not defined');
		}

		return false;
	}

	var i;
	var j;
	var allTables = null;
	var contentTables = null;

	allTables = document.getElementsByTagName('table');

	for (i = 0, j = 0; i < allTables.length; i++) {
		var tableID = new String(allTables.item(i).getAttribute('id'));
		if (tableID.substr(0, 12) == 'contentTable') {
			if (contentTables == null)
				contentTables = new Array();

			contentTables[j++] = allTables.item(i);
		}
	}

	if (contentTables == null) {
		if (contentTableDebug) {
			alert('no contentTable is defined');
		}
		return false;
	}

	var maxClientWidth = null;
	var newWidth       = null;
	var percentageBase = null;

	switch (contentTableResize.arguments.length) {
		default:
		case 3:		percentageBase = contentTableResize.arguments[2];
		case 2:		newWidth       = contentTableResize.arguments[1];
		case 1:		maxClientWidth = contentTableResize.arguments[0];
		case 0:		break;
	}

	if (maxClientWidth == null) {
		if (contentTableDebug) {
			alert('maxClientWidth (parameter no. 1) is not specified');
		}
		return false;
	}

	/* Set new width to maximum client width if not specified */
	if (newWidth == null)
		newWidth = maxClientWidth;

	/* By default no percentage base is used */
	if (percentageBase == null)
		percentageBase = 0;

	if (document.body.clientWidth > maxClientWidth) {
		for (i = 0; i < contentTables.length; i++) {
			var mulFactor;
			var origWidth;

			if (contentTablesOriginalWidth == null)
				contentTablesOriginalWidth = new Array();

			if (contentTablesOriginalWidth[i] != null) {
				origWidth = contentTablesOriginalWidth[i];
			} else {
				origWidth = contentTables[i].getAttribute('width');
				contentTablesOriginalWidth[i] = origWidth;
			}

			if (percentageBase > 0
					&& origWidth.charAt(origWidth.length - 1) == "%") {
				mulFactor  = origWidth.substr(0, origWidth.length - 1);
				mulFactor *= (100 / percentageBase);
				mulFactor /= 100;
			} else {
				mulFactor = 1;	
			}

			contentTables[i].setAttribute('width', newWidth * mulFactor);

			if (contentTableDebug != null && contentTableDebug) {
				alert("Table no. " + i
						+ "\nClient width is "
						+ document.body.clientWidth
						+ ".\nMaximal allowed width is "
						+ maxClientWidth
						+ ".\nChanging content table width from "
						+ origWidth
						+ " to "
						+ contentTables[i].width
						+ ".\nMul factor is "
						+ mulFactor
						+ ".\n");
			}
		}
	} else {
		if (contentTableDebug != null && contentTableDebug) {
			alert('No content table resizing needed.');
		}

		if (contentTablesOriginalWidth != null) {
			for (i = 0; i < contentTables.length; i++) {
				if (contentTablesOriginalWidth[i] != null) {
					contentTables[i].setAttribute('width', contentTablesOriginalWidth[i]);

					if (contentTableDebug != null && contentTableDebug) {
						alert("Table no. " + i
								+ "\nClient width is "
								+ document.body.clientWidth
								+ ".\nMaximal allowed width is "
								+ maxClientWidth
								+ ".\nReverting content table width back to "
								+ contentTables[i].width
								+ ".\n");
					}
				}
			}
		}
	}

	return true;
}


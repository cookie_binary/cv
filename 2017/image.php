<?php



$fn = $_GET['fn'];

if (file_exists($fn)) {

    $cacheFn = "cache/images/" . md5($fn);

    if (!file_exists($cacheFn)) {
        $img = imagecreatefromjpeg($fn);   // load the image-to-be-saved
        imagejpeg($img, $cacheFn, 50);
        exit;
    }

    header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + (60 * 60 * 24 * 12))); // 1 hour
    header('Content-Type: image/jpeg');
    readfile($cacheFn);
    exit;
}
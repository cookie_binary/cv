<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>Martin Osuský - Curriculum Vitae</title>
    <base href="/2017/">
    <link href='//fonts.googleapis.com/css?family=Istok+Web:400,700,400italic,700italic&subset=latin,latin-ext'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Cabin:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="styles/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/timeline.css"/>
    <link rel="stylesheet" type="text/css" href="styles/x.css">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/main.js"></script>
    <script src="js/timeline.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>
<body>
<div class="background"></div>
<div id="content">

    <?php include("layout/header.php") ?>


    <?php include("sections/skills.php") ?>
    <?php include("sections/timeline.php") ?>
    <?php include("sections/what-i-use.php") ?>


    <?php include("layout/footer.php") ?>


</div>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0]
        s1.async = true
        s1.src = 'https://embed.tawk.to/5d2365e222d70e36c2a4b8c4/default'
        s1.charset = 'UTF-8'
        s1.setAttribute('crossorigin', '*')
        s0.parentNode.insertBefore(s1, s0)
    })()
</script>
<!--End of Tawk.to Script-->


</body>
</html>

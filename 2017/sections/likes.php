
<section id="likes">
    <div class="page">

        <h2>
            I like</h2>

        <ul>
            <li>
                <img src="images/technologies.jpg">
                <h4>Technologies</h4>
                <ul>
                    <li>Programming</li>
                    <li>Machine Learning</li>
                    <li>AI</li>
                    <li>Algorithms</li>
                    <li>Frameworks</li>
                    <li>Science</li>
                    <li>Space</li>
                    <li>Computers</li>
                    <li>Audio</li>
                    <li>Video</li>
                </ul>
                <br clear="all">
            </li>
            <li>
                <img src="images/design.jpg">
                <h4>Design</h4>
                <ul>
                    <li>Web</li>
                    <li>UX</li>
                    <li>Photography</li>
                    <li>Art</li>
                    <li>PhotoShop</li>
                </ul>
                <br clear="all">
            </li>
            <li>
                <img src="images/music.jpg">
                <h4>Music</h4>
                <ul>
                    <li>Electronic</li>
                    <li>Producing</li>
                    <li>DJ-ing</li>
                    <li>House</li>
                    <li>DnB</li>
                    <li>Ableton</li>
                    <li>Drum'n'bass</li>
                    <li>Queen of the Stone Age
                    </li>
                </ul>
                <br clear="all">
            </li>
        </ul>

    </div>
    <br clear="all">
</section>



<section id="what-i-use">
    <div class="page">

        <h2>
            What I Use</h2>

        <ul>
            <li>
                <img src="images/whatiuse/laravel-logo.png">
                <p>
                    Laravel</p>
                </p>
            </li>
            <li>
                <img src="images/whatiuse/jquery-logo.png">
                <p>
                    JQuery</p>
            </li>
            <li>
                <img src="images/whatiuse/composer-logo.png">
                <p>
                    Composer</p>
            </li>
            <li>
                <img src="images/whatiuse/git-logo.png">
                <p>
                    Git</p>
            </li>
            <li>
                <img src="images/whatiuse/gulp-logo.png">
                <p>
                    Gulp</p>
            </li>
            <li>
                <img src="images/whatiuse/bower-logo.svg">
                <p>
                    Bower</p>
            </li>
            <li>
                <img src="images/whatiuse/phpstorm-logo.png">
                <p>
                    PhpStorm</p>
            </li>
            <li>
                <img src="images/whatiuse/jira-logo.png">
                <p>
                    Jira</p>
            </li>
            <li>
                <img src="images/whatiuse/phpunit-logo.gif">
                <p>
                    PhpUnit</p>
            </li>
            <li>
                <img src="images/whatiuse/bamboo-logo.png">
                <p>
                    Bamboo</p>
            </li>
            <li>
                <img src="images/whatiuse/lean-logo.png">
                <p>
                    LeanTesting</p>
            </li>
            <li>
                <img src="images/whatiuse/photoshop-logo.png">
                <p>
                    Photoshop</p>
            </li>
            <li>
                <img src="images/whatiuse/ubuntu-logo.png">
                <p>
                    Ubuntu</p>
            </li>
            <li>
                <img src="images/whatiuse/debian-logo.png">
                <p>
                    Debian</p>
            </li>
            <li>
                <img src="images/whatiuse/slack-logo.png">
                <p>
                    Slack</p>
            </li>
            <li>
                <img src="images/whatiuse/digitalocean-logo.png">
                <p>
                    Digital Ocean </p>
            </li>
            <li>
                <img src="images/whatiuse/aws.png" style="position: relative; top: -30px;">
                <p>
                    AWS </p>
            </li>
            <li>
                <img src="images/whatiuse/bootstrap-solid.svg">
                <p>
                    Bootstrap</p>
            </li>


            <li>
                <img src="images/whatiuse/datagrip-logo.png">
                <p>
                    DataGrip</p>
            </li>
            <li>
                <img src="images/whatiuse/docker-logo.png">
                <p>
                    Docker</p>
            </li>
            <li>
                <img src="images/whatiuse/logo-docker-compose-notext.png">
                <p>
                    Docker-compose</p>
            </li>
            <li>
                <img src="images/whatiuse/logo-kubernetes.png">
                <p>
                    Kubernetes</p>
            </li>
            <li>
                <img src="images/whatiuse/PyCharm_Logo.svg">
                <p>
                    PyCharm</p>
            </li>
            <li>
                <img src="images/whatiuse/ITerm2-icon.png">
                <p>
                    ITerm2</p>
            </li>
            <li>
                <img src="images/whatiuse/sourcetree-logo-852CEF45CF-seeklogo.com.png">
                <p>
                    SourceTree</p>
            </li>

        </ul>

    </div>
</section>


<section id="skills">
    <div class="page">
        <div style="display: flex; width: 100%">
            <div style="flex:2">

                <h2>
                    Skills</h2>

                <div class="tools_expert">
                    <div class="skill_main">
                        <div class="skill_item hidden">
                            <img src="images/php-logo.png">
                            <h4>PHP</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="95"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 95%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="skill_item hidden">
                            <img src="images/mysql-logo.png">
                            <h4>MySQL</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="90"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 90%;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="skill_item hidden">
                            <img src="images/html-logo-aa.png">
                            <img src="images/css-icon-aa.png">
                            <h4>HTML/CSS</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 80%;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="skill_item hidden">
                            <img src="images/JS-Logo.png">
                            <h4>Javascript</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 80%;"></div>
                                </div>
                            </div>
                        </div>


                        <div class="skill_item hidden">
                            <img src="images/Python-logo-notext.svg">
                            <h4>Python</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 60%;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="skill_item hidden">
                            <img src="images/nodejs-new-pantone-black.png">
                            <h4>NodeJS</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 75%;"></div>
                                </div>
                            </div>
                        </div>


                        <div class="skill_item hidden">
                            <img src="images/Tux.png">
                            <h4>Linux/Unix</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="88"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 88%;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="skill_item hidden">
                            <img src="images/go-programming-language.png">
                            <h4>GoLang</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 50%;"></div>
                                </div>
                            </div>
                        </div>

                        <div class="skill_item hidden">
                            <img src="images/postgresql-logo.png">
                            <h4>Postgres</h4>
                            <div class="progress_br">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="75"
                                         aria-valuemin="0"
                                         aria-valuemax="100" style="width: 80%;"></div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


            </div>
            <div id="im12">
                <div style="flex: 1; background: url('images/programming.jpg'); background-size: cover; background-position: center">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</section>


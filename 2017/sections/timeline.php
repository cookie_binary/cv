<section id="timeline">
    <div class="page">
        <h2>TimeLine</h2>
        <section id="cd-timeline" class="cd-container">

            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>Start</h4>
                        <p>
                            <span class="var">created_at:</span> 1981-09-10<br>
                            <span class="var">live_in:</span> Bratislava<br>
                        </p>
                    </div>

                    <div>
                        <img src="images/bratislava.jpg">
                    </div>
                    <div>
                        <span class="cd-date">September 1981</span>
                    </div>
                </div>
            </div>


            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>First Program</h4>
                        <p>
                            <span class="var">programming_language:</span> BASIC<br>
                            <span class="var">length:</span> about 300 lines<br>
                            <span class="var">platform:</span> Sinclear ZX Spectrum
                        </p>
                    </div>
                    <div>
                        <img src="images/basic-code.png">
                        <span class="cd-date">December 1993</span>
                    </div>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>Education</h4>
                        <p>
                            <span class="var">location:</span> Bratislava<br>
                            <span class="var">status:</span> Some unfinished technical oriented universities<br>
                        </p>
                    </div>
                    <div>
                        <span class="cd-date">2000 - 2004</span>
                    </div>
                </div>
            </div>


            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>meanwhile First Www</h4>
                        <p>
                            <span class="var">url:</span> Check <a href="http://onemindnation.wz.cz/"
                                                                   target="_blank">onemindnation.wz.cz</a><br>
                            <span class="var">technologies:</span> Html, Css, PHP, MySQL and Flash<br>
                        </p>
                    </div>
                    <div>
                        <a href="http://onemindnation.wz.cz/" target="_blank">
                            <img src="images/onemindnation-wz-cz-2x.jpg">
                        </a>
                        <span class="cd-date">February 2001</span>
                    </div>
                </div>
            </div>


            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>First Job</h4>
                        <p>
                            <span class="var">company:</span> Mediatech s.r.o<br>
                            <span class="var">job_description:</span> Web and e-shop administration and lightweight
                            development
                        </p>
                    </div>
                    <div>
                        <span class="cd-date">2004 - 2005</span>
                    </div>
                </div>

            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>Development</h4>
                        <p>
                            <span class="var">company:</span> Aston ITM s.r.o<br>
                            <span class="var">position:</span> Junior/mid developer<br>

                            <span class="var">job_description:</span> Programming web pages, web apps, portals and
                            intranets
                            for
                            third parties
                        </p>
                    </div>
                    <div>
                        <img src="images/programming.jpg">
                        <span class="cd-date">2005 - 2014</span>
                    </div>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>Much Experience</h4>
                        <p>
                            <span class="var">company:</span> Pricemania s.r.o.<br>
                            <span class="var">job_description:</span> Price comparator programming<br>
                            <span class="var">position:</span> Senior developer
                        </p>
                    </div>
                    <div>
                        <img src="images/pricemania.jpg">
                        <span class="cd-date">2014 - 2016</span>
                    </div>
                </div>
            </div>

            <div class="cd-timeline-block">
                <div class="cd-timeline-img tl-color7"></div>
                <div class="cd-timeline-content">
                    <div>
                        <h4>Current Job</h4>
                        <p>
                            <span class="var">company:</span> 01people <span style="font-size:0.7em">(old name: Wazzupa.Com)</span>
                            <br>
                            <span class="var">position:</span> Senior developer / Team leader
                            <span class="var">job_description:</span> Senior programming & leading, modeling and maintaining <br>

                            <span class="var">main technologies:</span>

                        </p>

                        <ul>
                            <li>Web techs (PHP, Html, CSS, etc.)</li>
                            <li>
                                UNIX/Linux
                            </li>
                            <li>
                                VMs/VPS
                            </li>
                            <li>
                                Docker "family" (compose, swarm, kubernetes)
                            </li>
                            <li>
                                NodeJS (singleton APIs, complex apps)
                            </li>    <li>
                                Python (Scripting, Django)
                            </li>
                            <li>
                                SQL family
                            </li>
                            <li>
                                services
                            </li>
                            <li>
                                etc.
                            </li>
                        </ul>
                        <Br><br>
                    </div>
                    <div>
                        <img src="images/php.jpg">
                        <span class="cd-date">since 2016</span>
                    </div>
                </div>
            </div>


        </section>

    </div>
</section>
